package altszama.orderEntry


enum class OrderEntryPaymentStatus {
  UNPAID,
  MARKED,
  CONFIRMED
}