package altszama.orderEntry

import altszama.dish.SideDish
import altszama.auth.User


data class EatingPersonEntry(
    val user: User,
    val comments: String,
    val sideDishes: List<SideDish>
) {
  companion object {
    fun create(orderEntry: OrderEntry): EatingPersonEntry {
      return EatingPersonEntry(orderEntry.user, orderEntry.additionalComments, orderEntry.chosenSideDishes)
    }
  }
}