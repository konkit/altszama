package altszama.orderEntry

import altszama.dish.Dish
import altszama.dish.DishRepository
import javax.validation.constraints.NotNull


data class OrderEntryUpdateCmd(
    @NotNull var id: String,
    @NotNull var orderId: String,
    @NotNull var dishId: String,
    var additionalComments: String,
    var sideDishesIds: List<String>
) {

  fun updateOrderEntry(orderEntryRepository: OrderEntryRepository,
                       dishRepository: DishRepository): OrderEntry {

    val orderEntry: OrderEntry = orderEntryRepository.findOne(id)
    val dish: Dish = dishRepository.findOne(dishId)

    orderEntry.dish = dish
    orderEntry.additionalComments = additionalComments

    val sideDishes = sideDishesIds.mapNotNull { sideDishId ->
      dish.sideDishes.find { sideDish ->
        sideDish.id == sideDishId
      }
    }

    orderEntry.chosenSideDishes = sideDishes

    return orderEntry
  }
}