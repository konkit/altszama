package altszama.orderEntry

import altszama.dish.Dish


data class GroupedOrderEntry(
    val dish: Dish,
    val price: Int,
    val eatingPeopleCount: Int,
    val eatingPeopleEntries: List<EatingPersonEntry>
)