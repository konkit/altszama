package altszama.orderEntry

import altszama.dish.Dish
import altszama.dish.DishRepository
import altszama.dish.SideDish
import altszama.order.Order
import altszama.order.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class OrderEntryController {

  @Autowired
  private lateinit var orderService: OrderService

  @Autowired
  private lateinit var orderEntryService: OrderEntryService

  @Autowired
  private lateinit var dishRepository: DishRepository


  data class OrderEntryCreateData(
    var order: Order,
    var allDishesInRestaurant: List<Dish>,
    var allDishesByCategory: Map<String, List<Dish>>,
    var dishIdToSideDishesMap: Map<String, List<SideDish>>
  )

  @RequestMapping(value = "/orders/{orderId}/create_entry.json")
  fun create(@PathVariable orderId: String): OrderEntryCreateData {
    val order = orderService.findOne(orderId)
    val allDishesInRestaurant = dishRepository.findByRestaurantId(order.restaurant.id)
    val allDishesInRestaurantByCategory = allDishesInRestaurant.groupBy { dish -> dish.category }
    val dishIdToSideDishesMap = orderEntryService.getDishToSideDishesMap(order.restaurant)

    return OrderEntryCreateData(order, allDishesInRestaurant, allDishesInRestaurantByCategory, dishIdToSideDishesMap)
  }

  @RequestMapping(value = "/order_entries/save")
  fun save(@RequestBody saveCmd: OrderEntrySaveCmd): OrderEntry {
    return orderEntryService.saveOrderEntry(saveCmd)
  }

  data class OrderEntryEditData(
    var order: Order,
    var allDishesInRestaurant: List<Dish>,
    var orderEntry: OrderEntry,
    var dishIdToSideDishesMap: Map<String, List<SideDish>>
  )

  @RequestMapping(value = "/order_entries/{orderEntryId}/edit_entry.json")
  fun edit(@PathVariable orderEntryId: String): OrderEntryEditData {
    val orderEntry = orderService.findOneEntry(orderEntryId)
    val order = orderEntry.order
    val dishesInRestaurant = dishRepository.findByRestaurantId(order.restaurant.id)
    val dishIdToSideDishesMap = orderEntryService.getDishToSideDishesMap(order.restaurant)

    return OrderEntryEditData(order, dishesInRestaurant, orderEntry, dishIdToSideDishesMap);
  }

  @RequestMapping(value = "/order_entries/update")
  fun update(@RequestBody updateCmd: OrderEntryUpdateCmd): OrderEntry {
    return orderEntryService.updateOrderEntry(updateCmd)
  }

  @RequestMapping(value = "/order_entries/{orderEntryId}/delete")
  fun delete(@PathVariable orderEntryId: String): String {
    orderEntryService.deleteOrderEntry(orderEntryId)
    return "OK"
  }

  @RequestMapping(value = "/order_entries/{orderEntryId}/mark_as_paid")
  fun setAsMarkedAsPaid(@PathVariable orderEntryId: String): String {
    orderEntryService.setAsMarkedAsPaid(orderEntryId)
    return "OK"
  }

  @RequestMapping(value = "/order_entries/{orderEntryId}/confirm_as_paid")
  fun setAsConfirmedAsPaid(@PathVariable orderEntryId: String): String {
    orderEntryService.setAsConfirmedAsPaid(orderEntryId)
    return "OK"
  }
}