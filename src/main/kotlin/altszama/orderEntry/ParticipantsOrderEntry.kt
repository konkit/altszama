package altszama.orderEntry

import altszama.dish.Dish
import altszama.dish.SideDish
import altszama.auth.User


data class ParticipantsOrderEntry(
    val id: String,
    val user: User,
    val dish: Dish,
    val sideDishes: List<SideDish>,
    val basePrice: Int,
    val finalPrice: Int,
    val comments: String,
    val paymentStatus: OrderEntryPaymentStatus
) {

  companion object {
    fun create(orderEntry: OrderEntry, finalPrice: Int, entriesCount: Int): ParticipantsOrderEntry {
      return ParticipantsOrderEntry(
          id = orderEntry.id,
          user = orderEntry.user,
          dish = orderEntry.dish,
          sideDishes = orderEntry.chosenSideDishes,
          basePrice = orderEntry.dish.price,
          finalPrice = finalPrice,
          comments = orderEntry.additionalComments,
          paymentStatus = orderEntry.paymentStatus
      )
    }
  }
}