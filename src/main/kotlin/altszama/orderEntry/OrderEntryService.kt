package altszama.orderEntry

import altszama.dish.DishRepository
import altszama.dish.SideDish
import altszama.restaurant.Restaurant
import altszama.auth.SpringSecurityService
import altszama.order.*
import altszama.validation.ValidationFailedException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.validation.Errors
import org.springframework.validation.Validator


@Service
class OrderEntryService {

  @Autowired
  private lateinit var springSecurityService: SpringSecurityService

  @Autowired
  private lateinit var orderRepository: OrderRepository

  @Autowired
  private lateinit var orderEntryRepository: OrderEntryRepository

  @Autowired
  private lateinit var dishRepository: DishRepository

  @Autowired
  private lateinit var validator: Validator


  fun saveOrderEntry(saveCmd: OrderEntrySaveCmd): OrderEntry {
    val orderEntry = saveCmd.getOrderEntry(orderRepository, dishRepository, springSecurityService)

    val errors: Errors = BeanPropertyBindingResult(orderEntry, "")

    if (orderEntry?.order == null) {
      errors.reject("orderEntry.wrongOrder", "Order is wrong or empty")
      throw ValidationFailedException(errors)
    }

    rejectIfAlreadyOrdered(errors, orderEntry.order)

    validator.validate(orderEntry, errors)

    if (errors.hasErrors()) {
      throw ValidationFailedException(errors)
    } else {
      orderEntryRepository.save(orderEntry)
      return orderEntry
    }
  }

  fun updateOrderEntry(updateCmd: OrderEntryUpdateCmd): OrderEntry {
    val orderEntry = updateCmd.updateOrderEntry(orderEntryRepository, dishRepository)

    val errors: Errors = BeanPropertyBindingResult(orderEntry, "")
    rejectIfAlreadyOrdered(errors, orderEntry.order)

    rejectIfNotOrderEntryCreator(errors, orderEntry)

    if (errors.hasErrors()) {
      throw ValidationFailedException(errors)
    } else {
      orderEntryRepository.save(orderEntry)
      return orderEntry
    }
  }

  fun deleteOrderEntry(orderEntryId: String) = orderEntryRepository.delete(orderEntryId)



  fun getDishToSideDishesMap(restaurant: Restaurant): Map<String, List<SideDish>> {
    val dishesFromRestaurant = dishRepository.findByRestaurantId(restaurant.id)
    return dishesFromRestaurant.map { dish -> dish.id to dish.sideDishes }.toMap()
  }

  fun setAsMarkedAsPaid(orderEntryId: String) {
    val entry = orderEntryRepository.findOne(orderEntryId)

    if (entry.paymentStatus != OrderEntryPaymentStatus.CONFIRMED) {
      entry.paymentStatus = OrderEntryPaymentStatus.MARKED
    }

    orderEntryRepository.save(entry)
  }

  fun setAsConfirmedAsPaid(orderEntryId: String) {
    val entry = orderEntryRepository.findOne(orderEntryId)

    entry.paymentStatus = OrderEntryPaymentStatus.CONFIRMED

    orderEntryRepository.save(entry)
  }

  private fun rejectIfAlreadyOrdered(errors: Errors, order: Order?) {
    if (order == null || order.orderState != OrderState.CREATED) {
      errors.reject("orderEntry.orderInProgress", "The order is already in progress")
      throw ValidationFailedException(errors)
    }
  }

  private fun rejectIfNotOrderEntryCreator(errors: Errors, orderEntry: OrderEntry?) {
    val currentUser = springSecurityService.currentUser()

    if (orderEntry == null || orderEntry.user != currentUser) {
      errors.reject("orderEntry.notOrderOwner", "You can edit only your own order entries.")
      throw ValidationFailedException(errors)
    }
  }
}