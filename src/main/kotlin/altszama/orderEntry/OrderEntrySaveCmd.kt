package altszama.orderEntry

import altszama.dish.DishRepository
import altszama.order.OrderRepository
import altszama.auth.SpringSecurityService
import org.bson.types.ObjectId
import javax.validation.constraints.NotNull


data class OrderEntrySaveCmd(
  @NotNull var orderId: String,
  @NotNull var dishId: String,
  var additionalComments: String,
  var sideDishesIds: List<String>
) {

  fun getOrderEntry(orderRepository: OrderRepository,
                    dishRepository: DishRepository,
                    springSecurityService: SpringSecurityService): OrderEntry? {

    val order = orderRepository.findOne(orderId)
    val dish = dishRepository.findOne(dishId)
    val currentUser = springSecurityService.currentUser()

    val sideDishes = this.sideDishesIds.mapNotNull { sideDishId ->
      dish.sideDishes.find { sideDish ->
        sideDish.id == sideDishId
      }
    }

    return OrderEntry(
      id = ObjectId().toHexString(),
      order = order,
      user = currentUser!!,
      dish = dish,
      additionalComments = this.additionalComments,
      chosenSideDishes = sideDishes
    )
  }
}