package altszama.restaurant

import altszama.dish.DishRepository
import altszama.order.OrderRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RestaurantService
@Autowired constructor(private val restaurantRepository: RestaurantRepository,
                       private val dishRepository: DishRepository,
                       private val orderRepository: OrderRepository) {

  fun findAll(): List<Restaurant> {
    return restaurantRepository.findAll()
  }

  fun deleteRestaurant(restaurantId: String) {
    val ordersFromRestaurant = orderRepository.findByRestaurantId(restaurantId)
    if (ordersFromRestaurant.isNotEmpty()) {
      throw Exception("There are orders from this restaurant")
    } else {
      val dishesToRemove = dishRepository.findByRestaurantId(restaurantId);
      dishesToRemove.forEach { dish ->
        dishRepository.delete(dish)
      }

      restaurantRepository.delete(restaurantId)
    }
  }

}
