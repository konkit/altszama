package altszama.restaurant;

import altszama.dish.Dish;
import altszama.dish.DishRepository;
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@RestController
class RestaurantController
@Autowired constructor(
    private val restaurantRepository: RestaurantRepository,
    private val restaurantService: RestaurantService,
    private val dishRepository: DishRepository,
    private val objectMapper: ObjectMapper
) {

  data class IndexJsonData(
    var restaurants: List<Restaurant>
  )

  @RequestMapping("/restaurants.json")
  fun indexJson(): IndexJsonData  {
    val restaurants: List<Restaurant> = restaurantRepository.findAll()

    return IndexJsonData(restaurants)
  }

  data class ShowJsonData(
    var restaurant: Restaurant,
    var dishes: List<Dish>,
    var dishesByCategory: Map<String, List<Dish>>
  )

  @RequestMapping("/restaurants/{restaurantId}/show.json")
  fun showJson(@PathVariable restaurantId: String): ShowJsonData {
    val restaurant = restaurantRepository.findById(restaurantId)!!
    val dishes = dishRepository.findByRestaurantId(restaurant.id)
    val dishesByCategory: Map<String, List<Dish>> = dishes.groupBy { dish -> dish.category }

    return ShowJsonData(restaurant, dishes, dishesByCategory)
  }

  @RequestMapping("/restaurants/save")
  fun saveRestaurant(@Valid @RequestBody restaurant: Restaurant, bindingResult: BindingResult, res: HttpServletResponse): List<String> {
    if (bindingResult.hasErrors()) {
      res.status = 400
      return bindingResult.allErrors.map { x -> x.defaultMessage }
    } else {
      restaurantRepository.insert(restaurant)
      res.status = 201
      return emptyList()
    }
  }

  data class EditJsonData(
    val id: String,
    val name: String,
    val address: String,
    val rating: Double,
    val telephone: String,
    val url: String
  )

  @RequestMapping("/restaurants/{restaurantId}/edit.json")
  fun editRestaurantJson(@PathVariable restaurantId: String): EditJsonData {
    val restaurant = restaurantRepository.findById(restaurantId)!!

    return EditJsonData(
      restaurant.id,
      restaurant.name,
      restaurant.address,
      restaurant.rating,
      restaurant.telephone,
      restaurant.url
    )
  }

  @RequestMapping("/restaurants/update")
  fun updateRestaurant(@RequestBody restaurant: Restaurant, bindingResult: BindingResult, res: HttpServletResponse): String {
    if (bindingResult.hasErrors()) {
      res.status = 400
      return "ERROR"
    } else {
      restaurantRepository.save(restaurant)
      return "OK"
    }
  }

    @RequestMapping("/restaurants/{restaurantId}/delete")
    fun deleteRestaurant(@PathVariable restaurantId: String, res: HttpServletResponse): String {
      val restaurant = restaurantRepository.findOne(restaurantId)

      if (restaurant == null ) {
        res.status = 400
        return "ERROR"
      } else {
        restaurantService.deleteRestaurant(restaurantId)
        return "OK"
      }
    }
}
