package altszama.validation;

import org.springframework.validation.Errors;

class ValidationFailedException : RuntimeException {
    var errors: Errors? = null

    constructor(errors: Errors) {
        this.errors = errors
    }
}
