package altszama.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@ControllerAdvice
class ValidationFailedExceptionHandler
@Autowired constructor(private val messageSource: MessageSource) {

  data class ErrorResult(val message: String)

  @ExceptionHandler(ValidationFailedException::class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  fun processValidationError(ex: ValidationFailedException): ErrorResult {
    val errors = ex.errors?.allErrors ?: emptyList()
    val errorMessages = errors.map { error -> getErrorMessage(error) }

    return ErrorResult(errorMessages.joinToString())
  }

  private fun getErrorMessage(error: ObjectError): String {
    return if (error is FieldError) {
      error.field + " " + messageSource.getMessage(error, null);
    } else {
      error.objectName + " " + messageSource.getMessage(error, null);
    }
  }
}
