package altszama.dish

import altszama.restaurant.RestaurantRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
class DishController
@Autowired constructor(
    private val restaurantRepository: RestaurantRepository,
    private val dishRepository: DishRepository,
    private val dishService: DishService
) {

  data class CreateJsonData(var categories: List<String>)

  @RequestMapping("/restaurants/{restaurantId}/dishes/create.json")
  fun createDishJson(@PathVariable restaurantId: String): CreateJsonData {
    val categories = dishService.allCategories(restaurantId)
    return CreateJsonData(categories)
  }

  @RequestMapping("/restaurants/{restaurantId}/dishes/save")
  fun saveDish(@PathVariable restaurantId: String, @Valid @RequestBody dish: Dish, br: BindingResult): String {
    val restaurant = restaurantRepository.findById(restaurantId)
    dish.restaurant = restaurant

    dishRepository.insert(dish)

    return "OK"
  }

  data class EditJsonData(var dish: Dish, var categories: List<String>)

  @RequestMapping("/restaurants/{restaurantId}/dishes/{dishId}/edit.json")
  fun editDishJson(@PathVariable restaurantId: String, @PathVariable dishId: String): EditJsonData {
    val categories = dishService.allCategories(restaurantId)
    return EditJsonData(dishRepository.findById(dishId)!!, categories)
  }

  @RequestMapping("/restaurants/{restaurantId}/dishes/update")
  fun updateDish(@PathVariable restaurantId: String, @Valid @RequestBody dish: Dish, br: BindingResult): String {
    val restaurant = restaurantRepository.findById(restaurantId)
    dish.restaurant = restaurant

    dishRepository.save(dish)

    return "OK"
  }

  @RequestMapping("/restaurants/{restaurantId}/dishes/{dishId}/delete")
  fun deleteDish(@PathVariable dishId: String): String {
    dishService.deleteDish(dishId)
    return "OK"
  }

  @RequestMapping("/dishes/{dishId}/side_dishes/{sideDishId}/delete")
  fun deleteSideDish(@PathVariable dishId: String, @PathVariable sideDishId: String): String {
    dishService.deleteSideDish(dishId, sideDishId)
    return "OK"
  }
}
