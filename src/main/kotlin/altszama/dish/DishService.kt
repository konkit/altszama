package altszama.dish

import altszama.orderEntry.OrderEntryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class DishService
@Autowired constructor(
    val dishRepository: DishRepository,
    val orderEntryRepository: OrderEntryRepository
) {

  fun allCategories(restaurantId: String): List<String> {
    return dishRepository.findByRestaurantId(restaurantId).map { dish -> dish.category }.distinct()
  }

  fun deleteDish(dishId: String) {
    if (orderEntryRepository.findByDishId(dishId).size > 0) {
      throw Exception("There are order entries using this dish!")
    }

    dishRepository.delete(dishId)
  }

  fun deleteSideDish(dishId: String, sideDishId: String) {
    val dish = dishRepository.findOne(dishId)

    if (dish != null) {
      dish.sideDishes = dish.sideDishes.filter { sd -> sd.id != sideDishId }
      dishRepository.save(dish)
    }
  }

}