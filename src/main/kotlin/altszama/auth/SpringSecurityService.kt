package altszama.auth

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class SpringSecurityService {

  @Autowired
  private lateinit var userRepository: UserRepository

  fun currentUser(): User? {
    val securityContext = SecurityContextHolder.getContext()
    val auth = securityContext.authentication

    val userId = auth.principal as String

    return userRepository.findOne(userId)
  }
}
