package altszama.order

import altszama.restaurant.RestaurantRepository
import altszama.auth.SpringSecurityService
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate
import java.time.LocalTime
import javax.validation.constraints.NotNull

data class OrderSaveCmd(
  @NotNull
  var restaurantId: String,

  @DateTimeFormat(pattern = "yyyy-MM-dd") @NotNull
  var orderDate: LocalDate,

  @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
  var timeOfOrder: LocalTime,

  var decreaseInPercent: Int = 0,
  var deliveryCostPerEverybody: Int = 0,
  var deliveryCostPerDish: Int = 0,
  var paymentByCash: Boolean = false,
  var paymentByBankTransfer: Boolean = false,
  var bankTransferNumber: String = ""
) {

  fun getOrder(restaurantRepository: RestaurantRepository,
               springSecurityService: SpringSecurityService): Order {

    val restaurant = restaurantRepository.findById(restaurantId)!!
    val currentUser = springSecurityService.currentUser()!!

    val newOrder = Order(
      restaurant = restaurant,
      orderCreator = currentUser,
      orderDate = orderDate
    )

    newOrder.timeOfOrder = timeOfOrder
    newOrder.timeOfDelivery = null
    newOrder.decreaseInPercent = decreaseInPercent
    newOrder.deliveryCostPerEverybody = deliveryCostPerEverybody
    newOrder.deliveryCostPerDish = deliveryCostPerDish
    newOrder.paymentByCash = paymentByCash
    newOrder.paymentByBankTransfer = paymentByBankTransfer
    newOrder.orderState = OrderState.CREATED
    newOrder.bankTransferNumber = bankTransferNumber

    return newOrder
  }

}