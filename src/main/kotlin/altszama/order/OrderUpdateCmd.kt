package altszama.order

import altszama.restaurant.RestaurantRepository
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate
import java.time.LocalTime
import javax.validation.constraints.NotNull


data class OrderUpdateCmd(
    @NotNull
    var orderId: String,

    @NotNull
    var restaurantId: String,

    @DateTimeFormat(pattern = "yyyy-MM-dd") @NotNull
    var orderDate: LocalDate,

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    var timeOfOrder: LocalTime,

    var decreaseInPercent: Int = 0,
    var deliveryCostPerEverybody: Int = 0,
    var deliveryCostPerDish: Int = 0,
    var paymentByCash: Boolean = false,
    var paymentByBankTransfer: Boolean = false,
    var bankTransferNumber: String
) {

  fun updateOrder(restaurantRepository: RestaurantRepository,
                  orderRepository: OrderRepository): Order {

    val existingOrder = orderRepository.findOne(orderId)

    val restaurant = restaurantRepository.findById(restaurantId)!!

    existingOrder.restaurant = restaurant
    existingOrder.orderDate = this.orderDate
    existingOrder.timeOfOrder = this.timeOfOrder
    existingOrder.decreaseInPercent = this.decreaseInPercent
    existingOrder.deliveryCostPerEverybody = this.deliveryCostPerEverybody
    existingOrder.deliveryCostPerDish = this.deliveryCostPerDish
    existingOrder.paymentByCash = this.paymentByCash
    existingOrder.paymentByBankTransfer = this.paymentByBankTransfer
    existingOrder.bankTransferNumber = this.bankTransferNumber

    return existingOrder
  }

}