package altszama.order

import altszama.orderEntry.ParticipantsOrderEntry


data class ShowJsonData(
    val order: Order,
    val orderEntries: List<ParticipantsOrderEntry>,
    val currentUserId: String
)