package altszama.order

import altszama.restaurant.Restaurant
import altszama.auth.User
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.format.annotation.DateTimeFormat
import javax.validation.constraints.NotNull
import java.time.LocalDate
import java.time.LocalTime


class Order(
  @DBRef
  @NotNull
  var restaurant: Restaurant,

  @DBRef
  @NotNull
  var orderCreator: User,

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  @NotNull
  var orderDate: LocalDate
) {

  var timeOfOrder: LocalTime? = null

  var timeOfDelivery: LocalTime? = null

  var orderState: OrderState = OrderState.CREATED

  var decreaseInPercent: Int = 0

  var deliveryCostPerEverybody: Int = 0

  var deliveryCostPerDish: Int = 0

  var paymentByCash: Boolean = true

  var paymentByBankTransfer: Boolean = true

  var bankTransferNumber: String = ""

  @Id
  var id: String = ObjectId().toHexString()

}
