package altszama.order

import altszama.restaurant.Restaurant
import altszama.restaurant.RestaurantService
import altszama.auth.SpringSecurityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.LocalTime


@RestController
class OrderController {

  @Autowired
  private lateinit var orderService: OrderService

  @Autowired
  private lateinit var restaurantService: RestaurantService

  @Autowired
  private lateinit var springSecurityService: SpringSecurityService


  @RequestMapping("/orders.json")
  fun index(): IndexJsonData {
    val todaysOrders = orderService.findByOrderDate(LocalDate.now())
    val currentUser = springSecurityService.currentUser()!!

    return orderService.createIndexJsonData(todaysOrders, currentUser)
  }

  data class AllOrdersJsonData(
    val allOrdersList: List<Order>
  )

  @RequestMapping("/orders/all.json")
  fun allOrders(): AllOrdersJsonData {
    val allOrders: List<Order> = orderService.allOrders()
    return AllOrdersJsonData(allOrders)
  }

  @RequestMapping("/orders/{orderId}/show.json")
  fun show(@PathVariable orderId: String): ShowJsonData {
    val order = orderService.findOne(orderId)
    val currentUserId = springSecurityService.currentUser()!!.id

    return orderService.createShowJsonData(order, currentUserId)
  }

  data class CreateJsonData(
    val restaurantsList: List<Restaurant>,
    val orderDate: LocalDate = LocalDate.now(),
    val timeOfOrder: LocalTime = LocalTime.of(10, 0)
  )

  @RequestMapping("/orders/create.json")
  fun create(): CreateJsonData = CreateJsonData(restaurantService.findAll())

  @RequestMapping("/orders/save")
  fun save(@RequestBody orderSaveCmd: OrderSaveCmd) = orderService.saveOrder(orderSaveCmd)

  data class EditJsonData(
    val order: Order,
    val restaurantsList: List<Restaurant>
  )

  @RequestMapping("/orders/{orderId}/edit.json")
  fun edit(@PathVariable orderId: String) = EditJsonData(orderService.findOne(orderId), restaurantService.findAll())

  @RequestMapping("/orders/update")
  fun update(@RequestBody orderUpdateCmd: OrderUpdateCmd) = orderService.updateOrder(orderUpdateCmd)

  @RequestMapping("/orders/{orderId}/delete")
  fun delete(@PathVariable orderId: String) = orderService.deleteOrder(orderId)

  @RequestMapping("/orders/{orderId}/order_view.json")
  fun orderViewJson(@PathVariable orderId: String): OrderViewJsonData {
    orderService.setAsOrdering(orderId)
    return orderService.createOrderViewJsonData(orderId)
  }

  @RequestMapping("/orders/{orderId}/set_as_created")
  fun setAsCreated(@PathVariable orderId: String): ResponseEntity<String> {
    orderService.setAsCreated(orderId)
    return ResponseEntity("{}", HttpStatus.OK)
  }

  @RequestMapping("/orders/{orderId}/set_as_ordering")
  fun setAsOrdering(@PathVariable orderId: String): ResponseEntity<String> {
    orderService.setAsOrdering(orderId)
    return ResponseEntity("{}", HttpStatus.OK)
  }

  data class SetAsOrderedData(
    val approxTimeOfDelivery: String?
  )

  @RequestMapping("/orders/{orderId}/set_as_ordered")
  fun setAsOrdered(@PathVariable orderId: String, @RequestBody requestData: SetAsOrderedData): ResponseEntity<String> {
    orderService.setAsOrdered(orderId, requestData.approxTimeOfDelivery)
    return ResponseEntity("{}", HttpStatus.OK)
  }

  @RequestMapping("/orders/{orderId}/set_back_as_ordered")
  fun setAsOrdered(@PathVariable orderId: String): ResponseEntity<String> {
    orderService.setBackAsOrdered(orderId)
    return ResponseEntity("{}", HttpStatus.OK)
  }

  @RequestMapping("/orders/{orderId}/set_as_delivered")
  fun setAsDelivered(@PathVariable orderId: String): ResponseEntity<String> {
    orderService.setAsDelivered(orderId)
    return ResponseEntity("{}", HttpStatus.OK)
  }

  @RequestMapping("/orders/{orderId}/set_as_rejected")
  fun setAsRejected(@PathVariable orderId: String): ResponseEntity<String> {
    orderService.setAsRejected(orderId)
    return ResponseEntity("{}", HttpStatus.OK)
  }

}
