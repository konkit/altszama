package altszama.order

import altszama.orderEntry.GroupedOrderEntry


data class OrderViewJsonData(
    val order: Order,
    val groupedEntries: List<GroupedOrderEntry>,
    val allEatingPeopleCount: Int,
    val basePriceSum: Int,
    val totalPrice: Int
)