package altszama.order

import altszama.orderEntry.OrderEntry


data class IndexJsonData(
    val ordersList: List<Order>,
    val createdOrders: List<Order>,
    val orderingOrders: List<Order>,
    val orderedOrders: List<Order>,
    val deliveredOrders: List<Order>,
    val currentOrderEntries: List<OrderEntry>
)