package altszama.order

import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeParseException
import java.time.temporal.ChronoUnit
import altszama.notification.NotificationService
import altszama.restaurant.RestaurantRepository
import altszama.auth.SpringSecurityService
import altszama.auth.User
import altszama.dish.SideDish
import altszama.orderEntry.*
import altszama.validation.ValidationFailedException
import org.funktionale.option.Option
import org.funktionale.option.toOption
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.Optional


@Service
class OrderService {

  @Autowired
  private lateinit var springSecurityService: SpringSecurityService

  @Autowired
  private lateinit var orderRepository: OrderRepository

  @Autowired
  private lateinit var restaurantRepository: RestaurantRepository

  @Autowired
  private lateinit var orderEntryRepository: OrderEntryRepository

  @Autowired
  private lateinit var notificationService: NotificationService

  @Autowired
  private lateinit var validator: Validator


  fun allOrders(): List<Order> = orderRepository.findAll()

  fun findOne(orderId: String): Order = orderRepository.findOne(orderId)

  fun findOneEntry(orderEntryId: String): OrderEntry = orderEntryRepository.findOne(orderEntryId)

  fun findByOrderDate(orderDate: LocalDate): List<Order> = orderRepository.findByOrderDate(orderDate)

  fun saveOrder(saveCmd: OrderSaveCmd): Order {
    val order = saveCmd.getOrder(restaurantRepository, springSecurityService)

    val errors = BeanPropertyBindingResult(order, "")

    rejectIfBankTransferAccountMissing(errors, order)

    validator.validate(order, errors)

    if (errors.hasErrors()) {
      throw ValidationFailedException(errors)
    } else {
      orderRepository.insert(order)
      return order
    }
  }

  fun updateOrder(updateCmd: OrderUpdateCmd): Order {
    val order = updateCmd.updateOrder(restaurantRepository, orderRepository)

    val errors = BeanPropertyBindingResult(order, "")

    rejectIfNotOrderCreator(errors, order)
    rejectIfBankTransferAccountMissing(errors, order)

    validator.validate(order, errors)

    if (errors.hasErrors()) {
      throw ValidationFailedException(errors)
    } else {
      orderRepository.save(order)
      return order
    }
  }

  fun setAsCreated(orderId: String) {
    val errors = BeanPropertyBindingResult("order", "Order")

    orderRepository.findOne(orderId).toOption()
      .map { order ->
        rejectIfNotOrderCreator(errors, order)
        order.orderState = OrderState.CREATED
        order.timeOfDelivery = null
        orderRepository.save(order)
      }
  }

  fun setAsOrdering(orderId: String) {
    val errors: Errors = BeanPropertyBindingResult("order", "")
    val order: Order = orderRepository.findOne(orderId)

    rejectIfNotOrderCreator(errors, order)

    val orderEntries: List<OrderEntry> = orderEntryRepository.findByOrderId(order.id)

    if (orderEntries.isEmpty()) {
//      errors.reject("order.noOrderEntries", "There are no order entries")
//      throw ValidationFailedException(errors)
    } else {
      order.orderState = OrderState.ORDERING
      orderRepository.save(order)
    }
  }

  fun setAsOrdered(orderId: String, approxTimeOfDelivery: String?) {
    val errors = BeanPropertyBindingResult("order", "")
    val order = orderRepository.findOne(orderId)
    rejectIfNotOrderCreator(errors, order)

    val orderEntries: List<OrderEntry> = orderEntryRepository.findByOrderId(order.id)

    if (orderEntries.isEmpty()) {
      errors.reject("order.noOrderEntries", "There are no order entries")
      throw ValidationFailedException(errors)
    }

    order.orderState = OrderState.ORDERED
    order.timeOfOrder = currentLocalTime().truncatedTo(ChronoUnit.MINUTES)

    try {
      order.timeOfDelivery = LocalTime.parse(approxTimeOfDelivery)
    } catch(e: DateTimeParseException) {
        // Do nothing - time of delivery will be blank
    }

    val formatter = DateTimeFormatter.ISO_LOCAL_TIME//.withZone(ZoneId.of("Europe/Warsaw"))
    val eta = order.timeOfDelivery?.format(formatter) ?: "Undefined"

    orderRepository.save(order)
    notificationService.notificateOrdered(order, eta)
  }

  fun setBackAsOrdered(orderId: String) {
    val errors = BeanPropertyBindingResult("order", "")
    val order = orderRepository.findOne(orderId)

    rejectIfNotOrderCreator(errors, order)

    order.orderState = OrderState.ORDERED
    orderRepository.save(order)
    notificationService.notificateDelivered(order)
  }

  fun setAsDelivered(orderId: String) {
    val errors = BeanPropertyBindingResult("order", "")
    val order = orderRepository.findOne(orderId)

    rejectIfNotOrderCreator(errors, order)

    order.orderState = OrderState.DELIVERED
    order.timeOfDelivery = currentLocalTime().truncatedTo(ChronoUnit.MINUTES)
    orderRepository.save(order)
    notificationService.notificateDelivered(order)
  }

  fun setAsRejected(orderId: String) {
    val errors = BeanPropertyBindingResult("order", "")
    val order = orderRepository.findOne(orderId)

    rejectIfNotOrderCreator(errors, order)

    order.orderState = OrderState.REJECTED
    orderRepository.save(order)
    notificationService.notificateRejected(order)
  }

   fun deleteOrder(orderId: String) {
    val removedOrder = orderRepository.findOne(orderId)
    val errors = BeanPropertyBindingResult(removedOrder, "")

    rejectIfNotOrderCreator(errors, removedOrder)

    orderEntryRepository.deleteByOrderId(orderId)
    orderRepository.delete(orderId)
  }

  fun createIndexJsonData(todaysOrders: List<Order>, currentUser: User): IndexJsonData {
    val orderTypeToOrder = todaysOrders.groupBy { order -> order.orderState }

    val currentOrderEntries = orderEntryRepository.findByUser(currentUser)
        .filter { orderEntry -> todaysOrders.any { order -> order.id == orderEntry.order.id } }

    return IndexJsonData(
      todaysOrders,
      orderTypeToOrder[OrderState.CREATED] ?: emptyList(),
      orderTypeToOrder[OrderState.ORDERING] ?: emptyList(),
      orderTypeToOrder[OrderState.ORDERED] ?: emptyList(),
      orderTypeToOrder[OrderState.DELIVERED] ?: emptyList(),
      currentOrderEntries
    )
  }

  fun createShowJsonData(order: Order, currentUserId: String): ShowJsonData {
    val entries = orderEntryRepository.findByOrderId(order.id)
    val entriesByUser = entries.groupBy { e -> e.user }

    val entriesCount = entries.size
    val usersCount = entriesByUser.keys.size

    val participantsUserEntries = entriesByUser
      .flatMap { userToEntries -> userToEntries.value }
      .map { orderEntry ->
        val numberOfDishesForUser = entriesByUser.get(orderEntry.user)!!.size

        val basePrice = orderEntry.priceWithSidedishes()

        val decreaseAmount: Int = ( basePrice * (order.decreaseInPercent / 100.0) ).toInt()
        val deliveryCostPerOrder = (order.deliveryCostPerEverybody / usersCount) / numberOfDishesForUser
        val deliveryCostPerEntry = order.deliveryCostPerDish

        val finalPrice = basePrice - decreaseAmount + deliveryCostPerOrder + deliveryCostPerEntry

        ParticipantsOrderEntry.create(orderEntry, finalPrice, entriesCount)
      }

    return ShowJsonData(order, participantsUserEntries, currentUserId)
  }

  fun createOrderViewJsonData(orderId: String): OrderViewJsonData {
    val order = orderRepository.findOne(orderId)
    val entries = orderEntryRepository.findByOrderId(orderId)

    val basePriceSum = entries.map(OrderEntry::priceWithSidedishes).sum()

    val decrease = (order.decreaseInPercent / 100.0 * basePriceSum).toInt()
    val deliveryCostPerDishes = entries.size * order.deliveryCostPerDish

    val orderTotalPrice = basePriceSum - decrease + order.deliveryCostPerEverybody + deliveryCostPerDishes

    val groupedUserEntries = entries
      .groupBy { entry -> entry.dish }
      .map { (dish, entriesForDish) ->
        val eatingPersonEntries = entriesForDish.map(EatingPersonEntry.Companion::create)
        val priceSumForDish = entriesForDish.map(OrderEntry::priceWithSidedishes).sum()

        GroupedOrderEntry(dish, priceSumForDish, entriesForDish.size, eatingPersonEntries)
      }

    return OrderViewJsonData(order, groupedUserEntries, entries.size, basePriceSum, orderTotalPrice)
  }

  private fun rejectIfNotOrderCreator(errors: Errors, order: Order?) {
    val currentUser = springSecurityService.currentUser()

    if (order == null || order.orderCreator != currentUser) {
      errors.reject("orderEntry.notOrderOwner", "You can edit only your orders.")
      throw ValidationFailedException(errors)
    }
  }

  private fun rejectIfBankTransferAccountMissing(errors: Errors, order: Order?) {
    if (order == null || ( order.paymentByBankTransfer && order.bankTransferNumber.isBlank() ) ) {
      errors.reject("orderEntry.missingBankTransferNumber", "Bank transfer number is missing.")
      throw ValidationFailedException(errors)
    }
  }

  private fun currentLocalTime() = LocalTime.now(ZoneId.of("Europe/Warsaw"))



}

