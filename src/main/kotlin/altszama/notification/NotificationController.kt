package altszama.notification;

import altszama.auth.SpringSecurityService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
class NotificationController {

  @Autowired
  private lateinit var subscriberRepository: PushNotSubscriptionRepository

  @Autowired
  private lateinit var springSecurityService: SpringSecurityService

  private val logger = LoggerFactory.getLogger(NotificationController::class.java)

  @RequestMapping("/notification/subscribe")
  fun addSubscriber(@RequestBody subscriberParams: PushNotifSubscription): String {
    val currentUser = springSecurityService.currentUser()

    if (currentUser != null) {
      val currentSubscriber = subscriberRepository.findByUserId(currentUser.id)
          ?: PushNotifSubscription()

      currentSubscriber.authKey = subscriberParams.authKey
      currentSubscriber.endpoint = subscriberParams.endpoint
      currentSubscriber.p256dhKey = subscriberParams.p256dhKey
      currentSubscriber.userId = currentUser.id

      subscriberRepository.save(currentSubscriber)

      logger.info("Subscriber added - " + subscriberParams)
    } else {
      logger.info("User not authenticated, subscription not added")
    }

    return "OK"
  }

  @RequestMapping("/notification/unsubscribe/}")
  fun removeSubscriber(): String {
    val currentUser = springSecurityService.currentUser()!!

    val subscr = subscriberRepository.findByUserId(currentUser.id)
    if (subscr != null) {
      subscriberRepository.delete(subscr)
    }

    return "OK"
  }
}
