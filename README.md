## How to run dev environment ?

1. Make sure you have node.js and NPM installed

2. Put those values as environment variables:
* gcmSenderId - API number for receiving push notifications
* gcmServerKey - API key for sending Push Notifications
* spring.data.mongodb.database - MongoDB database name
* googleClientId - Google's ClientID value
* googleClientSecret - Google's Client Secret value
* BACKEND_URL - for dev environment its http://localhost:8081
3. Run the project from IDE (so the backend part is running)
and the frontend part by running "runVueDevServer" Gradle task 


## How to run production environment ?

1. Make sure you have node.js and NPM is installed

2. Put those values as environment variables:
* gcmSenderId - API number for receiving push notifications
* gcmServerKey - API key for Push Notifications
* spring.data.mongodb.database - MongoDB database name
* googleClientId - Google's ClientID value
* googleClientSecret - Google's Client Secret value

3. Running "build" project should already depend on vueProd task, which build frontend
assets and put them in backend's app resources.
